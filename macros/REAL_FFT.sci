//  Copyright (C) DIGITEO 2010 - Yann COLLETTE (yann.collette@scilab.org)

function [x,y,typ]=REAL_FFT(job,arg1,arg2)
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1);
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1);
case 'getoutputs' then
  x=[];y=[];typ=[];
case 'getorigin' then
  [x,y]=standard_origin(arg1);
case 'set' then
  x        = arg1;
  graphics = arg1.graphics;
  exprs    = graphics.exprs;
  model    = arg1.model;

  while %t do
    [ok, buffer_size, padding_value, exprs]=scicos_getvalue(...
	'Set Real FFT parameters',...
	['Buffer size';
	 'Padding value'],...
	 list('vec',1,'vec',1),...
	 exprs)
    if ~ok then break,end //user cancel modification
    mess=[]
    if buffer_size>2 then
      mess=[mess;'Buffer size must be > 1';' ']
      ok=%f
    end
    if ~ok then
      message(['Some specified values are inconsistent:';
               ' ';mess]);
    end
    if ok then
      [model,graphics,ok]=set_io(model,graphics,list([-1 1],1),list(),[],[])
    end
    
    if ok then
      rpar = [padding_value];
      ipar = [buffer_size];
      model.rpar  = rpar;
      model.ipar  = ipar;
      model.label = nom;
      graphics.id = nom;
      graphics.exprs = exprs;
      x.graphics = graphics;
      x.model    = model;
      break;
    end
  end
case 'define' then
  buffer_size   = 10;
  padding_value = 0.0;

  model = scicos_model()
  model.sim = list('real_fft_xcos',4)
  model.in  = 1;
  model.in2 = 1;
  model.rpar = [padding_value];
  model.ipar = [buffer_size];
  model.blocktype = 'c';
  model.dep_ut    = [%t %f];

  exprs = [string(buffer_size);
	   string(padding_value)];

  gr_i = ['thick=xget(''thickness'');xset(''thickness'',2);';
          'xrect(orig(1)+sz(1)/10,orig(2)+(1-1/10)*sz(2),sz(1)*8/10,sz(2)*8/10);';
          'xset(''thickness'',thick)'];

  x = standard_define([2 2],model,exprs,gr_i);
end
endfunction
