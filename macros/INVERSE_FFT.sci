//  Copyright (C) DIGITEO 2010 - Yann COLLETTE (yann.collette@scilab.org)

function [x,y,typ]=INVERSE_FFT(job,arg1,arg2)
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1);
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1);
case 'getoutputs' then
  x=[];y=[];typ=[];
case 'getorigin' then
  [x,y]=standard_origin(arg1);
case 'set' then
  x        = arg1;
  graphics = arg1.graphics;
  exprs    = graphics.exprs;
  model    = arg1.model;

  [model,graphics,ok]=set_io(model,graphics,list([-1 1],1),list(),[],[])
    
case 'define' then
  model = scicos_model()
  model.sim = list('inverse_fft_xcos',4)
  model.in  = -1;
  model.in2 = 1;
  model.blocktype = 'c';
  model.dep_ut    = [%t %f];

  exprs = [];

  gr_i = ['thick=xget(''thickness'');xset(''thickness'',2);';
          'xrect(orig(1)+sz(1)/10,orig(2)+(1-1/10)*sz(2),sz(1)*8/10,sz(2)*8/10);';
          'xset(''thickness'',thick)'];

  x = standard_define([2 2],model,exprs,gr_i);
end
endfunction
