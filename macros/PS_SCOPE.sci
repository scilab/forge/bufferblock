//  Copyright (C) DIGITEO 2010 - Yann COLLETTE (yann.collette@scilab.org)

function [x,y,typ] = PS_SCOPE(job,arg1,arg2)
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1);
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1);
case 'getoutputs' then
  x=[];y=[];typ=[];
case 'getorigin' then
  [x,y]=standard_origin(arg1);
case 'set' then
  x        = arg1;
  graphics = arg1.graphics;
  exprs    = graphics.exprs;

  model = arg1.model;

  while %t do
    [ok, curves_label, curves_color, win_id, buffer_size, win_pos, win_dim, ...
     adap_amp, heritance, has_legend, has_semilogx, has_grid, ...
     amp_min, amp_max, refresh_period, freq_min, freq_max, padding_value, ...
     sampling_freq, exprs] = scicos_getvalue(...
	'Set PS Scope parameters',...
	['Curves label';
	 'Curves color';
	 'Window ID';
	 'Buffer size';
	 'Window position';
	 'Window dimension';
	 'Adaptive amplitude';
         'Accept herited events 0/1';
	 'Legend ?'; 
         'Semilog X ?';
         'Grid ?';
         'Amplitude min';
         'Amplitude max';
         'Refresh_period';
         'Frequency min';
         'Frequency max';
         'Padding_value';
         'Sampling frequency'],...
	 list('str',-1, ... // Curves label
              'vec',-1, ... // Curves color
              'vec',1,  ... // Window ID
              'vec',1,  ... // Buffer size
              'vec',-1, ... // Window position
              'vec',-1, ... // Window dimension
              'vec',1,  ... // Adaptive amplitude
              'vec',1,  ... // Herited events
              'vec',1,  ... // Legend ?
              'vec',1,  ... // Semilog X ?
              'vec',1,  ... // Grid X ?
              'vec',1,  ... // Amplitude min
              'vec',1,  ... // Amplitude max
              'vec',1,  ... // Refresh period
              'vec',1,  ... // Frequency min
              'vec',1,  ... // Frequency max
              'vec',1,  ... // Padding value
              'vec',1), ... // Sampling freq
 	 exprs);
    if ~ok then break,end //user cancel modification
    mess=[]
    if size(win_pos,'*')<>0 & size(win_pos,'*')<>2 then
      mess=[mess;'Window position must be [] or a 2 vector';' ']
      ok=%f
    end
    if size(win_dim,'*')<>0 & size(win_dim,'*')<>2 then
      mess=[mess;'Window dim must be [] or a 2 vector';' ']
      ok=%f
    end
    if win_ID<-1 then
      mess=[mess;'Window number can''t be  < -1';' ']
      ok=%f
    end
    if refresh_period<=0 then
      mess=[mess;'Refresh period must be positive';' ']
      ok=%f
    end
    if buffer_size<2 then
      mess=[mess;'Buffer size must be at least 2';' ']
      ok=%f
    end
    if amp_min>=amp_max then
      mess=[mess;'Amplitude max must be greater than amplitude min';' ']
      ok=%f
    end
    if freq_min>=freq_max then
      mess=[mess;'Frequency max must be greater than frequency min';' ']
      ok=%f
    end
    if ~or(heritance==[0 1]) then
      mess=[mess;'Accept herited events must be 0 or 1';' ']
      ok=%f
    end
    if ~or(adap_amp==[0 1]) then
      mess=[mess;'Adaptive amplitude must be 0 or 1';' ']
      ok=%f
    end
    if ~or(has_legend==[0 1]) then
      mess=[mess;'Legend must be 0 or 1';' ']
      ok=%f
    end
    if ~or(has_semilogx==[0 1]) then
      mess=[mess;'Semilogx must be 0 or 1';' ']
      ok=%f
    end
    if ~or(has_grid==[0 1]) then
      mess=[mess;'Grid must be 0 or 1';' ']
      ok=%f
    end
    if ~ok then
      message(['Some specified values are inconsistent:';
	         ' ';mess])
	   end
    if ok then
      [model,graphics,ok]=set_io(model,graphics,list([-1 1],1),list(),ones(1-heritance,1),[])
    end
    
    if ok then
      if win_pos==[] then win_pos=[-1;-1];end
      if win_dim==[] then win_dim=[-1;-1];end
      rpar = [amp_min; ...
              amp_max; ...
              refresh_period; ...
              freq_min; ...
              freq_max; ...
              padding_value; ...
              sampling_freq];
      ipar = [size(curves_label,'*'); ...
              length(curves_label); ...
              str2code(strcat(curves_label)); ...
              curves_color; ...
              win_id; ...
              buffer_size; ...
              win_pos; ...
              win_dim; ...
              adap_amp; ...
              has_legend; ...
              has_semilogx; ...
              has_grid];
      model.rpar  = rpar;
      model.ipar  = ipar;
      model.evtin = ones(1-heritance,1);
      model.label = nom;
      graphics.id = nom;
      graphics.exprs = exprs;
      x.graphics     = graphics;
      x.model        = model;
      break;
    end
  end
case 'define' then
  win_id  = -1;
  win_dim = [600;400]
  win_pos = [-1;-1]
  curves_color = [1;3;5];
  curves_label = ['curve 1';'curve 2';'curve 3'];
  buffer_size  = 128;
  adap_amp     = 1;
  has_legend   = 0;
  has_semilogx = 1;
  has_grid     = 1;
  heritance    = 0;

  amp_min        = -60;
  amp_max        = 20;
  refresh_period = 0.3;
  freq_min       = 10;
  freq_max       = 10000;
  padding_value  = 0.0;
  sampling_freq  = 20000;

  model = scicos_model();
  model.sim   = list('ps_scope_xcos',4);
  model.in    = -1;
  model.in2   = 1;
  model.evtin = 1;
  model.rpar  = [amp_min; ...
                 amp_max; ...
                 refresh_period; ...
                 freq_min; ...
                 freq_max; ...
                 padding_value; ...
                 sampling_freq];
  model.ipar = [size(curves_label,'*'); ...
                length(curves_label); ...
                str2code(strcat(curves_label)); ...
                curves_color; ...
                win_id; ...
                buffer_size; ...
                win_pos; ...
                win_dim; ...
                adap_amp; ...
                has_legend; ...
                has_semilogx; ...
                has_grid];
  model.blocktype = 'c';
  model.dep_ut = [%t %f];

  exprs = [strcat(curves_label,' ');
           strcat(string(curves_color), ' ');
           sci2exp([]);
           string(buffer_size);
           sci2exp(win_pos);
           sci2exp(win_dim);
           string(adap_amp);
           string(heritance);
           string(has_legend);
           string(has_semilogx);
           string(has_grid);
           string(amp_min);
           string(amp_max);
           string(refresh_period)
           string(freq_min);
           string(freq_max);
           string(padding_value);
           string(sampling_freq)];

  gr_i = ['thick=xget(''thickness'');xset(''thickness'',2);';
          'xrect(orig(1)+sz(1)/10,orig(2)+(1-1/10)*sz(2),sz(1)*8/10,sz(2)*8/10);';
          'xset(''thickness'',thick)'];

  x = standard_define([2 2],model,exprs,gr_i);
end
endfunction
