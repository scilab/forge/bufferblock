// Copyright (C) 2008 - INRIA
// Copyright (C) 2009 - DIGITEO

// This file is released into the public domain

mode(-1);
lines(0);

TOOLBOX_NAME  = "scibuffervect";
TOOLBOX_TITLE = "sciBufferVect";
toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab's version
// =============================================================================

try
	v = getversion("scilab");
catch
	error(gettext("Scilab 5.2 or more is required."));
end

if v(2) < 2 then
	// new API in scilab 5.2
	error(gettext('Scilab 5.2 or more is required.'));  
end

// Check development_tools module avaibility
// =============================================================================

if ~with_module('development_tools') then
  error(msprintf(gettext('%s module not installed."),'development_tools'));
end

// Action
// =============================================================================

tbx_builder_macros(toolbox_dir);
tbx_builder_src(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

// Build xcos palette
// =============================================================================

loadScicosLibs();

getd('macros');

xpal = xcosPal("Buffer");
File = toolbox_dir + '/images/fft_scope.jpg';
xpal = xcosPalAddBlock(xpal, 'FFT_SCOPE', File, File);
File = toolbox_dir + '/images/ps_scope.jpg';
xpal = xcosPalAddBlock(xpal, 'PS_SCOPE', File, File);
File = toolbox_dir + '/images/buffer_vect.jpg';
xpal = xcosPalAddBlock(xpal, 'BUFFER_VECT', File, File);
File = toolbox_dir + '/images/real_fft.jpg';
xpal = xcosPalAddBlock(xpal, 'REAL_FFT', File, File);
File = toolbox_dir + '/images/inverse_fft.jpg';
xpal = xcosPalAddBlock(xpal, 'INVERSE_FFT', File, File);
File = toolbox_dir + '/images/window_func.jpg';
xpal = xcosPalAddBlock(xpal, 'WINDOW_FUNC', File, File);

//YC: not finished yet 
// File = toolbox_dir + '/images/vector_scope.jpg';
//xpal = xcosPalAddBlock(xpal, VECTOR_SCOPE, File, File);

xcosPalExport(xpal, toolbox_dir + '/Buffer.xpal');

// Clean variables
// =============================================================================

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE xpal;
