/*
 * Copyright (C) 2010 - DIGITEO - Yann COLLETTE
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <MALLOC.h>
#include <scicos_block4.h>
#include <localization.h>
#include "FFT.h"

void inverse_fft_xcos(scicos_block *block,int flag)
{
  /* input  1: the real part of the fft */
  /* input  2: the imag part of the fft */
  /* output 1: the real part of the signal */
  /* output 1: the imag part of the signal */

  int i = 0;
  int nsamples = GetInPortRows(block,1);
  float * in_real_fft = NULL;
  float * in_imag_fft = NULL;
  float * out_real_signal = NULL;
  float * out_imag_signal = NULL;

  switch(flag)
    {
    case Initialization:
      /* the workspace is used to store previous values */
      break;
    case Ending:
      break;
    case DerivativeState:
    case StateUpdate:
      /* during state update, we fill the buffer and shift the values */
    case OutputUpdate:
      /* during output update, we transform the buffer into an output vector */
      
      if ((in_real_fft = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "inverse_fft_xcos");
	  return;
	}
      
      if ((in_imag_fft = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "inverse_fft_xcos");
	  return;
	}

      if ((out_real_signal = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "inverse_fft_xcos");
	  return;
	}

      if ((out_imag_signal = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "inverse_fft_xcos");
	  return;
	}
      
      for(i=0;i<nsamples;i++) 
        {
          in_real_fft[i] = (float)((double *)GetInPortPtrs(block,1))[i];
          in_imag_fft[i] = (float)((double *)GetInPortPtrs(block,2))[i];
          out_real_signal[i] = 0;
          out_imag_signal[i] = 0;
        }

      FFT(nsamples, 1, in_real_fft, in_imag_fft, out_real_signal, out_imag_signal);
      
      for(i=0;i<nsamples;i++) 
        {
          ((double *)GetOutPortPtrs(block,1))[i] = (double)out_real_signal[i];
          ((double *)GetOutPortPtrs(block,2))[i] = (double)out_imag_signal[i];
        }

      if (in_real_fft)     FREE(in_real_fft);
      if (in_imag_fft)     FREE(in_imag_fft);
      if (out_real_signal) FREE(out_real_signal);
      if (out_imag_signal) FREE(out_imag_signal);

      break;
    }
}
