/*
 * Copyright (C) 2010 - DIGITEO - Yann COLLETTE
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <stdio.h>
#include "CurrentObjectsManagement.h"
#include "scicos.h"
#include "scoMemoryScope.h"
#include "scoWindowScope.h"
#include "scoMisc.h"
#include "scoGetProperty.h"
#include "scoSetProperty.h"
#include "scicos_block4.h"
#include "SetJavaProperty.h"
#include "MALLOC.h"
#include "dynlib_scicos_blocks.h"

#define MAX(A,B) ((A<B)?B:A)
#define MIN(A,B) ((A<B)?A:B)

/* IPAR content
   ipar[0] = curve color
   ipar[1] = win_id
   ipar[2:3] = [win_pos win_pos]
   ipar[4:5] = [win_dim win_dim]
   ipar[6] = adaptive_amp
   ipar[7] = grid */

int parse_ipar_vector_scope(scicos_block * block, int ** color, int * win_id, int ** win_pos, 
                            int ** win_dim, int * adaptive_amp, int * grid)
{
  int * ipar = GetIparPtrs(block);

  (*color)[0]     = ipar[0];
  (*win_id)       = ipar[1];
  (*win_pos)[0]   = ipar[2];
  (*win_pos)[1]   = ipar[3];
  (*win_dim)[0]   = ipar[4];
  (*win_dim)[1]   = ipar[5];
  (*adaptive_amp) = ipar[6];
  (*grid)         = ipar[7];

  return 0;
}

/* RPAR content
   rpar[0] = amp_min
   rpar[1] = amp_max
   rpar[2] = refresh_period
   rpar[3] = t_min
   rpar[4] = t_max */

int parse_rpar_vector_scope(scicos_block * block, double * amp_min, double * amp_max, double * refresh_period, 
                            double * t_min, double * t_max)
{
  double * rpar = GetRparPtrs(block);

  (*amp_min)        = rpar[0];
  (*amp_max)        = rpar[1];
  (*refresh_period) = rpar[2];
  (*t_min)          = rpar[3];
  (*t_max)          = rpar[4];

  return 0;
}

/* This structure will handle all the intermediate memory needed
    for:
    - the scope (the scope data are stored in pScopeMemory
    - the input signal (it needs to be converted to float) */

struct vector_scope_memory
{
  ScopeMemory * pScopeMemory;
  double * input_signal;
  double   amp_min;
  double   amp_max;
  double   t_min;
  double   t_max;
  double   refresh_period;
  int    * win_pos;
  int    * win_dim;
  int      win;
  int      window_type;
  int      adaptive_amp;
  int      grid;
  int    * color;
};

/** \fn vector_scope_draw(scicos_block * block, ScopeMemory ** pScopeMemory, int firstdraw)
    \brief Function to draw or redraw the window
*/

void vector_scope_draw(scicos_block * block, struct vector_scope_memory ** pVScopeMemory, int firstdraw)
{
  int    i = 0, ierror = 0;
  int    dimension = 0;
  int    line_size = 2;
  int    number_of_subwin = 0;
  int    number_of_curves_by_subwin[1];
  char * label = NULL;
  void * block_work_fake = NULL;

  if (firstdraw == 1)
    {
      (*pVScopeMemory) = (struct vector_scope_memory *)MALLOC(1*sizeof(struct vector_scope_memory));
      
      (*pVScopeMemory)->win_pos = (int *)MALLOC(2*sizeof(int));
      (*pVScopeMemory)->win_dim = (int *)MALLOC(2*sizeof(int));
      (*pVScopeMemory)->color   = (int *)MALLOC(1*sizeof(int));

      /* Retrieving IPAR and RPAR Parameters */
      
      ierror = parse_ipar_vector_scope(block, &((*pVScopeMemory)->color), &((*pVScopeMemory)->win), &((*pVScopeMemory)->win_pos),
                                       &((*pVScopeMemory)->win_dim), &((*pVScopeMemory)->adaptive_amp), &((*pVScopeMemory)->grid));

      if (ierror)
        {
          Coserror("%s: Memory allocation problem\n", "vector_scope_xcos");
          return;
        }

      ierror = parse_rpar_vector_scope(block, &((*pVScopeMemory)->amp_min), &((*pVScopeMemory)->amp_max), &((*pVScopeMemory)->refresh_period), 
                                       &((*pVScopeMemory)->t_min), &((*pVScopeMemory)->t_max));
      
      if (ierror)
        {
          Coserror("%s: Memory allocation problem\n", "vector_scope_xcos");
          return;
        }
    }

  /* Here, we store the maximum number of curves by subwin we can plot.
      This is the size of the number of colors stored in the colors list.
      If we have more curves, they will be printed using black color */

  number_of_curves_by_subwin[0] = 1;
  number_of_subwin = 1;
  dimension        = 2; // 2D graph

  label = GetLabelPtrs(block);

  /* Allocating memory */
  if (firstdraw == 1)
    {
      (*pVScopeMemory)->input_signal = (double *)MALLOC(GetInPortRows(block,1)*sizeof(double));
      if ((*pVScopeMemory)->input_signal==NULL)
        {
          Coserror("%s: Memory allocation problem\n", "vector_scope_xcos");
          FREE((*pVScopeMemory)->win_pos);
          FREE((*pVScopeMemory)->win_dim);
          FREE((*pVScopeMemory)->color);
          return;
        }

      /* block_work_fake is here to "fill" a missing parameter which is not used anymore
         because we store the memory in a special structure */
      scoInitScopeMemory(&block_work_fake,&((*pVScopeMemory)->pScopeMemory), number_of_subwin, number_of_curves_by_subwin);

      /* We set the buffers for LongDraw and ShortDraw */
      scoSetLongDrawSize((*pVScopeMemory)->pScopeMemory, 0,GetInPortRows(block,1));
      scoSetShortDrawSize((*pVScopeMemory)->pScopeMemory,0,GetInPortRows(block,1));
      scoSetPeriod((*pVScopeMemory)->pScopeMemory,0,(*pVScopeMemory)->t_max+(*pVScopeMemory)->t_min);
    }

  /* Creation of the Scope */
  scoInitOfWindow((*pVScopeMemory)->pScopeMemory, dimension, (*pVScopeMemory)->win, (*pVScopeMemory)->win_pos, 
                  (*pVScopeMemory)->win_dim, &((*pVScopeMemory)->t_min), &((*pVScopeMemory)->t_max), 
                  &((*pVScopeMemory)->amp_min), &((*pVScopeMemory)->amp_max), NULL, NULL);

  if (scoGetScopeActivation((*pVScopeMemory)->pScopeMemory) == 1)
    {
      scoAddTitlesScope((*pVScopeMemory)->pScopeMemory, label, "t.", "amp.",NULL);
      scoAddCoupleOfPolylines((*pVScopeMemory)->pScopeMemory,(*pVScopeMemory)->color);
    }

  if (scoGetPointerScopeWindow((*pVScopeMemory)->pScopeMemory) != NULL)
    {
      sciSetJavaUseSingleBuffer(scoGetPointerScopeWindow((*pVScopeMemory)->pScopeMemory), TRUE);
    }
 
  *block->work = *pVScopeMemory;
}

/** \fn void vector_scope(scicos_block * block,int flag)
    \brief the computational function
    \param block A pointer to a scicos_block
    \param flag An int which indicates the state of the block (init, update, ending)
*/
void vector_scope_xcos(scicos_block * block,int flag)
{
  struct vector_scope_memory * pVScopeMemory = NULL;
  int      i = 0, j = 0;
  double   t = 0, amp_tmp, amp_min, amp_max;
  scoGraphicalObject pLongDraw;
  scoGraphicalObject pAxes;
  int      line_size = 2;
  void  * block_work_fake = NULL;
  sciPointObj * Leg = NULL;

  switch(flag) 
    {
    case Initialization:
      {
	vector_scope_draw(block, &pVScopeMemory, 1);

        /* Init the time section */
        pVScopeMemory->pScopeMemory->d_last_scope_update_time = get_scicos_time();

	break;
      }
    
    case OutputUpdate:
      {
        pVScopeMemory = (struct vector_scope_memory *)*block->work;

        /* during state update, we fill the buffer and shift the values */
        for(i=0;i<GetInPortRows(block,1);i++) pVScopeMemory->input_signal[i] = ((double *)GetRealInPortPtrs(block,1))[i];

        if (scoGetScopeActivation(pVScopeMemory->pScopeMemory) == 1)
          {
	    t = get_scicos_time();

            if ((t - pVScopeMemory->pScopeMemory->d_last_scope_update_time > pVScopeMemory->refresh_period))
              {
                /* We force a redraw to clean the preceding curve */
                vector_scope_draw(block,&pVScopeMemory,0);
                
                pLongDraw  = scoGetPointerLongDraw(pVScopeMemory->pScopeMemory,0,0);
                if (pLongDraw)
                  {
                    if (pVScopeMemory->adaptive_amp)
                      {
                        amp_min = pVScopeMemory->input_signal[0];
                        amp_max = amp_min;
                        
                        for(i=0; i<GetInPortRows(block,1); i++)
                          {
                            if (pVScopeMemory->input_signal[i]<amp_min) amp_min = pVScopeMemory->input_signal[i];
                            if (pVScopeMemory->input_signal[i]>amp_max) amp_max = pVScopeMemory->input_signal[i];
                          }
                            
                        pAxes = scoGetPointerAxes(pVScopeMemory->pScopeMemory,0);
                        pSUBWIN_FEATURE(pAxes)->SRect[2] = amp_min;
                        pSUBWIN_FEATURE(pAxes)->SRect[3] = amp_max;
                        
                        for(i=0; i<GetInPortRows(block,1); i++)
                          {
                            pPOLYLINE_FEATURE(pLongDraw)->pvx[i] = (pVScopeMemory->t_max - pVScopeMemory->t_min)*i / 
                              (GetInPortRows(block,1)-1)+pVScopeMemory->t_min;
                            pPOLYLINE_FEATURE(pLongDraw)->pvy[i] = (double)pVScopeMemory->input_signal[i];
                            pPOLYLINE_FEATURE(pLongDraw)->n1 = i+1;
                          }
                      }
                    
                    if (pVScopeMemory->grid)
                      {
                        pSUBWIN_FEATURE(pAxes)->grid[0] = 1;
                        pSUBWIN_FEATURE(pAxes)->grid[1] = 1;
                        pSUBWIN_FEATURE(pAxes)->grid[2] = -1;
                      }
                    
                    forceRedraw(pAxes);
                  }
                
                pVScopeMemory->pScopeMemory->d_last_scope_update_time = get_scicos_time();
                scoDrawScopeAmplitudeTimeStyle(pVScopeMemory->pScopeMemory, pVScopeMemory->pScopeMemory->d_last_scope_update_time);
              }
          }

	break;
      }
    case Ending:
      {
        pVScopeMemory = (struct vector_scope_memory *)*block->work;

        FREE(pVScopeMemory->input_signal);
        FREE(pVScopeMemory->color);

	if (scoGetScopeActivation(pVScopeMemory->pScopeMemory) == 1)
	  {
            /* Check if figure is still opened, otherwise, don't try to destroy it again. */
            scoGraphicalObject figure = scoGetPointerScopeWindow(pVScopeMemory->pScopeMemory);
            if (figure != NULL)
              {
                clearUserData(figure);
                /* restore double buffering */
                sciSetJavaUseSingleBuffer(figure, FALSE);
                scoDelCoupleOfPolylines(pVScopeMemory->pScopeMemory);
              }
	  }
        
        /* block_work_safe is here to add a missing parameter in scoFreeScopeMemory
           We store another kind of structure in block->work: a struct vector_scope_memory */
        block_work_fake = (int *)MALLOC(1*sizeof(int));
	scoFreeScopeMemory(&block_work_fake, &(pVScopeMemory->pScopeMemory));
        break;  
      }
    }
}
