/*
 * Copyright (C) 2010 - DIGITEO - Yann COLLETTE
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <MALLOC.h>
#include "scicos.h"
#include "scicos_block4.h"
#include "localization.h"

void buffer_vect_xcos(scicos_block *block,int flag)
{
  /* ipar[0] = buffer_size */
  /* rpar[0] = padding value */
  int    i             = 0;
  int    nsamples      = GetIparPtrs(block)[0];
  double padding_value = GetRparPtrs(block)[0];
  /* state initialization */
  switch(flag)
    {
    case Initialization:
      /* the workspace is used to store previous values */
      if ((GetWorkPtrs(block) = (double *)MALLOC(sizeof(double)*nsamples))== NULL ) 
	{
	  Coserror("%s: Memory allocation problem\n", "buffer_vect_xcos");
	  return;
	}

      for(i=0;i<nsamples;i++) ((double *)GetWorkPtrs(block))[i] = padding_value;
      break;
    case Ending:
      FREE(*block->work);
      break;
    case DerivativeState:
    case StateUpdate:
      /* during state update, we fill the buffer and shift the values */

      for(i=1;i<nsamples;i++) ((double *)GetWorkPtrs(block))[i-1] = ((double *)GetWorkPtrs(block))[i];
      ((double *)GetWorkPtrs(block))[nsamples-1] = ((double *)GetRealInPortPtrs(block,1))[0];
      break;
    case OutputUpdate:
      /* during output update, we transform the buffer into an output vector */
      
      for(i=0;i<nsamples;i++) ((double *)GetOutPortPtrs(block,1))[i] = ((double *)GetWorkPtrs(block))[i];
      break;
    }
}
