/*
 * Copyright (C) 2010 - DIGITEO - Yann COLLETTE
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <string.h>
#include <MALLOC.h>
#include "scicos_block4.h"
#include "localization.h"
#include "FFT.h"

void window_func_xcos(scicos_block *block,int flag)
{
  /* ipar[0] = buffer_size */
  /* ipar[1] = window type - 0: rectangular, 1: bartlett, 2: hamming, 3: hanning 4: blackman */
  /* rpar[0] = padding value */
  /* rpar[1] = alpha parameter */

  int     i             = 0;
  int     nsamples      = block->ipar[0];
  int     window_type   = block->ipar[1];
  double  padding_value = block->rpar[0];
  double  alpha         = block->rpar[1];
  float * out_signal    = NULL;

  switch(flag)
    {
    case Initialization:
      /* the workspace is used to store previous values */
      if ((GetWorkPtrs(block) = (float *)MALLOC(sizeof(float)*nsamples)) == NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "window_func_xcos");
	  return;
	}

      for(i=0;i<nsamples;i++) ((float *)GetWorkPtrs(block))[i] = (float)padding_value;
      break;
    case Ending:
      FREE(GetWorkPtrs(block));
      break;
    case DerivativeState:
    case StateUpdate:
      /* during state update, we fill the buffer and shift the values */
      for(i=1;i<nsamples;i++) ((float *)GetWorkPtrs(block))[i-1] = ((float *)GetWorkPtrs(block))[i];
      ((float *)GetWorkPtrs(block))[nsamples-1] = (float)((double *)GetInPortPtrs(block,1))[0];
      break;
    case OutputUpdate:
      /* during output update, we transform the buffer into an output vector */
      if ((out_signal = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "window_func_xcos");
	  return;
	}
      memcpy(out_signal, (float *)GetWorkPtrs(block), nsamples*sizeof(float));

      WindowFunc((windowfunc_t)window_type, nsamples, out_signal, (float)alpha);

      for(i=0;i<nsamples;i++) ((double *)GetOutPortPtrs(block,1))[i] = (double)out_signal[i];

      if (out_signal) FREE(out_signal);
    }
}
