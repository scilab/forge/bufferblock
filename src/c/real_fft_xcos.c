/*
 * Copyright (C) 2010 - DIGITEO - Yann COLLETTE
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <MALLOC.h>
#include <scicos_block4.h>
#include <localization.h>
#include "FFT.h"

void real_fft_xcos(scicos_block *block,int flag)
{
  /* ipar[0] = buffer_size */
  /* rpar[0] = padding value */
  /* input  1: the real signal */
  /* output 1: the real part of the fft */
  /* output 2: the imag part of the fft */

  int       i = 0;
  int       nsamples      = GetIparPtrs(block)[0];
  double    padding_value = GetRparPtrs(block)[0];
  float  *  out_real_fft  = NULL;
  float  *  out_imag_fft  = NULL;

  switch(flag)
    {
    case Initialization:
      /* the workspace is used to store previous values */
      if ((GetWorkPtrs(block) = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "real_fft_xcos");
	  return;
	}
      
      for(i=0;i<nsamples;i++) ((float *)GetWorkPtrs(block))[i] = (float)padding_value;
      break;
    case Ending:
      FREE(*block->work);
      break;
    case DerivativeState:
    case StateUpdate:
      /* during state update, we fill the buffer and shift the values */
      
      for(i=1;i<nsamples;i++) ((float *)GetWorkPtrs(block))[i-1] = ((float *)GetWorkPtrs(block))[i];
      ((float *)GetWorkPtrs(block))[nsamples-1] = (float)((double *)GetInPortPtrs(block,1))[0];
    case OutputUpdate:
      /* during output update, we transform the buffer into an output vector */
      
      if ((out_real_fft = (float *)MALLOC(sizeof(float)*nsamples/2))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "real_fft_xcos");
	  return;
	}
      
      if ((out_imag_fft = (float *)MALLOC(sizeof(float)*nsamples/2))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "real_fft_xcos");
	  return;
	}
      
      RealFFT(nsamples, GetWorkPtrs(block), out_real_fft, out_imag_fft);
      
      for(i=0;i<nsamples/2;i++) 
        {
          ((double *)GetOutPortPtrs(block,1))[i] = (double)out_real_fft[i];
          ((double *)GetOutPortPtrs(block,2))[i] = (double)out_imag_fft[i];
        }

      if (out_real_fft) FREE(out_real_fft);
      if (out_imag_fft) FREE(out_imag_fft);
      break;
    }
}
